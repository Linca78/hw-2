            let body = document.getElementsByTagName('body')[0];
            let table = document.createElement('table');
            let tbdy = document.createElement('tbody');
            for (let i = 0; i < 30; i++) {
                let tr = document.createElement('tr');
                for (let j = 0; j < 30; j++) {
                    let td = document.createElement('td');
                        td.setAttribute('class', 'white');
                        tr.appendChild(td);
                }
                tbdy.appendChild(tr);
            }
            table.appendChild(tbdy);
            body.appendChild(table);
      

        document.querySelectorAll('table td').forEach(e => e.addEventListener('click', function(event) {
            if (this.classList.value == 'white') {
                this.classList.remove('white');
                this.classList.add('black');
            } else {
                this.classList.remove('black');
                this.classList.add('white');
            }
            event.stopPropagation();
        }));

        
        window.addEventListener("click", function() {
            let whiteTd = document.querySelectorAll('.white');
            let blackTd = document.querySelectorAll('.black');
			
            whiteTd.forEach(function(item, i, whiteTd) {
                item.classList.remove('white');
                item.classList.add('green');
            });

            blackTd.forEach(function(item, i, blackTd) {
                item.classList.remove('black');
                item.classList.add('white');
            });
        });
   